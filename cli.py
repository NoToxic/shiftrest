import uvicorn
from dotenv import load_dotenv
import os


if __name__ == '__main__':
    load_dotenv()
    uvicorn.run('main:app', reload=False, host=os.getenv("HOST"))
