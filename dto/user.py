from pydantic import BaseModel
from .salary import Salary


class UserBase(BaseModel):
    login: str


class UserCred(UserBase):
    password: str


class UserRole(UserBase):
    is_superuser: bool = False


class User(UserCred, UserRole, Salary):
    pass
