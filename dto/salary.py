import datetime
from decimal import Decimal
from pydantic import BaseModel


class Salary(BaseModel):
    salary: Decimal = None
    next_update: datetime.date = None
