import os
from fastapi import FastAPI, Request, Response
from routers import user as UserRouter
from database import init_db, get_db
from services import user as UserService
from dto import user as UserDTO
from security import jwt_token

app = FastAPI()
app.include_router(UserRouter.router)
app.include_router(UserRouter.auth_router)


@app.middleware("http")
async def add_jwt_lifetime_check(r: Request, call_next):
    if r.url.path not in jwt_token.jwt_routes:
        response = await call_next(r)
        return response

    bearer = r.headers.get("Authorization")
    if bearer is None:
        return Response("Invalid token", status_code=401)
    _, token = bearer.split()

    payload = jwt_token.decode(token)

    if payload is None:
        return Response("Lifetime is expired", status_code=401)

    response = await call_next(r)

    return response


@app.on_event('startup')
async def on_startup():
    await init_db()

    try:
        admin = UserDTO.User(login=os.getenv("ADMIN_LOGIN"), password=os.getenv("ADMIN_PASS"), is_superuser=True)
        async for db in get_db():
            await UserService.create_user(admin, db)
    except Exception:
        print("Admin already exists")
