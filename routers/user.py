from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.ext.asyncio import AsyncSession
from database import get_db

from services import user as UserService
from dto import user as UserDTO, token as TokenDTO, salary as SalaryDTO

router = APIRouter(
    prefix="/users",
    tags=["users"]
)
auth_router = APIRouter(
    prefix="/auth",
    tags=["users"]
)


@auth_router.post("/", response_model=TokenDTO.Token)
async def auth(
        data: UserDTO.UserCred,
        db: AsyncSession = Depends(get_db)
):
    if not await UserService.verify(data, db):
        raise HTTPException(status_code=401)

    current_user = await UserService.get_user(data.login, db)

    return UserService.create_access_token(current_user)


@router.post("/", response_model=UserDTO.UserRole, status_code=201)
async def create_user(
        data: UserDTO.User,
        db: AsyncSession = Depends(get_db),
        user: UserDTO.UserRole = Depends(UserService.jwt_to_dto)
):
    if user is None:
        raise HTTPException(status_code=401)

    if not user.is_superuser:
        raise HTTPException(status_code=403)

    if not (await UserService.get_user(data.login, db) is None):
        raise HTTPException(status_code=409)

    return await UserService.create_user(data, db)


@router.delete("/{login}", status_code=204)
async def delete_user(
        login: str,
        db: AsyncSession = Depends(get_db),
        user: UserDTO.UserRole = Depends(UserService.jwt_to_dto)
):
    if user is None:
        raise HTTPException(status_code=401)

    if not user.is_superuser:
        raise HTTPException(status_code=403)

    if await UserService.get_user(login, db) is None:
        raise HTTPException(status_code=400)

    await UserService.delete_user(login, db)


@router.get("/{login}", response_model=UserDTO.User, response_model_exclude={"password"})
async def get_user(
        login: str,
        db: AsyncSession = Depends(get_db),
        user: UserDTO.UserRole = Depends(UserService.jwt_to_dto)
):
    if user is None:
        raise HTTPException(status_code=401)

    if not (login == user.login or user.is_superuser):
        raise HTTPException(status_code=403)

    if await UserService.get_user(login, db) is None:
        raise HTTPException(status_code=400)

    return await UserService.get_user(login, db)

