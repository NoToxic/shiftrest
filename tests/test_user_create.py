import json

import requests
import os
from dotenv import load_dotenv

load_dotenv()

auth_url = f"http://{os.getenv('HOST')}:8000/auth"
user_url = f"http://{os.getenv('HOST')}:8000/users"
port = os.getenv("")
admin_pass = os.getenv("ADMIN_PASS")
admin_login = os.getenv("ADMIN_LOGIN")


def test_positive_create_user():
    response = requests.post(auth_url, json={"login": admin_login, "password": admin_pass})
    json_data = json.loads(str(response.content, encoding='utf-8'))
    token = json_data.get("access_token")
    new_user = {
        "salary": 10_000,
        "next_update": "2023-07-10",
        "login": "user",
        "is_superuser": False,
        "password": "user"
    }
    response = requests.post(user_url, headers={"Authorization": f"Bearer {token}"}, json=new_user)
    assert response.status_code == 201


def test_conflict_create_user():
    response = requests.post(auth_url, json={"login": admin_login, "password": admin_pass})
    json_data = json.loads(str(response.content, encoding='utf-8'))
    token = json_data.get("access_token")
    new_user = {
        "salary": 10_000,
        "next_update": "2023-07-10",
        "login": "user",
        "is_superuser": False,
        "password": "user"
    }
    response = requests.post(user_url, headers={"Authorization": f"Bearer {token}"}, json=new_user)
    assert response.status_code == 409


def test_negative_create_user():
    response = requests.post(auth_url, json={"login": "user", "password": "user"})
    json_data = json.loads(str(response.content, encoding='utf-8'))
    token = json_data.get("access_token")
    new_user = {
        "salary": 10_000,
        "next_update": "2023-07-10",
        "login": "user",
        "is_superuser": False,
        "password": "user"
    }
    response = requests.post(user_url, headers={"Authorization": f"Bearer {token}"}, json=new_user)
    assert response.status_code == 403


def test_negative_delete_user():
    response = requests.post(auth_url, json={"login": "user", "password": "user"})
    json_data = json.loads(str(response.content, encoding='utf-8'))
    token = json_data.get("access_token")

    response = requests.delete(user_url + "/user", headers={"Authorization": f"Bearer {token}"})
    assert response.status_code == 403


def test_delete_user():
    response = requests.post(auth_url, json={"login": admin_login, "password": admin_pass})
    json_data = json.loads(str(response.content, encoding='utf-8'))
    token = json_data.get("access_token")

    response = requests.delete(user_url + "/user", headers={"Authorization": f"Bearer {token}"})
    assert response.status_code == 204
