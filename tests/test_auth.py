import requests
import os
from dotenv import load_dotenv

load_dotenv()

url = f"http://{os.getenv('HOST')}:8000/auth"
port = os.getenv("")
admin_pass = os.getenv("ADMIN_PASS")
admin_login = os.getenv("ADMIN_LOGIN")


def test_positive_auth():
    response = requests.post(url, json={"login": admin_login, "password": admin_pass})
    assert response.status_code == 200


def test_negative_auth_pass():
    response = requests.post(url, json={"login": admin_login, "password": "wrong password"})
    assert response.status_code == 401


def test_empty_user_auth_login():
    response = requests.post(url, json={"login": "wrong user", "password": admin_pass})
    assert response.status_code == 401
