import json

import requests
import os
from dotenv import load_dotenv

load_dotenv()

auth_url = f"http://{os.getenv('HOST')}:8000/auth"
user_url = f"http://{os.getenv('HOST')}:8000/users"
port = os.getenv("")
admin_pass = os.getenv("ADMIN_PASS")
admin_login = os.getenv("ADMIN_LOGIN")

first_user = {
    "salary": 10_000,
    "next_update": "2023-07-10",
    "login": "first_user",
    "is_superuser": False,
    "password": "first_user"
}

second_user = {
    "salary": 10_000,
    "next_update": "2023-07-10",
    "login": "second_user",
    "is_superuser": False,
    "password": "second_user"
}


def get_access_token(login, password):
    response = requests.post(auth_url, json={"login": login, "password": password})
    json_data = json.loads(str(response.content, encoding='utf-8'))
    token = json_data.get("access_token")
    return token


def create_user(new_user: dict, token):
    requests.post(user_url, headers={"Authorization": f"Bearer {token}"}, json=new_user)


def delete_user(user: str, token):
    requests.delete(user_url + user, headers={"Authorization": f"Bearer {token}"})


def test_positive_get_user_by_admin():
    token = get_access_token(admin_login, admin_pass)
    create_user(first_user, token)
    response = requests.get(user_url + f"/{first_user.get('login')}", headers={"Authorization": f"Bearer {token}"})
    assert response.status_code == 200
    requests.delete(user_url + f"/{first_user.get('login')}", headers={"Authorization": f"Bearer {token}"})


def test_positive_get_user_by_user():
    admin_token = get_access_token(admin_login, admin_pass)
    create_user(first_user, admin_token)

    user_token = get_access_token(first_user.get("login"), first_user.get("password"))
    response = requests.get(user_url + f"/{first_user.get('login')}", headers={"Authorization": f"Bearer {user_token}"})

    assert response.status_code == 200
    requests.delete(user_url + f"/{first_user.get('login')}", headers={"Authorization": f"Bearer {admin_token}"})


def test_negative_get_user_by_user():
    admin_token = get_access_token(admin_login, admin_pass)
    create_user(first_user, admin_token)
    create_user(second_user, admin_token)

    user_token = get_access_token(first_user.get("login"), first_user.get("password"))
    response = requests.get(user_url + f"/{second_user.get('login')}", headers={"Authorization": f"Bearer {user_token}"})

    assert response.status_code == 403
    requests.delete(user_url + f"/{first_user.get('login')}", headers={"Authorization": f"Bearer {admin_token}"})
    requests.delete(user_url + f"/{second_user.get('login')}", headers={"Authorization": f"Bearer {admin_token}"})
