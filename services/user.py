from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy import select, delete
from dto import user as UserDTO, token as TokenDTO
from fastapi import Request
from models.user import User
from security.password_hash import myctx as hash_context
from security import jwt_token


async def verify(current_user: UserDTO.UserCred, db: AsyncSession) -> bool:
    query = select(User).where(User.login == current_user.login)
    user = await db.execute(query)
    user = user.scalar_one_or_none()
    if user is None:
        return False
    return hash_context.verify(current_user.password, user.password)


def create_access_token(data: UserDTO.User) -> TokenDTO.Token:
    user: UserDTO.UserRole = UserDTO.UserRole(**data.dict())
    return TokenDTO.Token(access_token=jwt_token.encode(user))


async def jwt_to_dto(r: Request) -> UserDTO.UserRole | None:
    bearer = r.headers.get("Authorization")
    if bearer is None:
        return None

    try:
        _, token = bearer.split()
    except ValueError:
        return None

    payload = jwt_token.decode(token)

    if payload is None:
        return None

    return UserDTO.UserRole(**payload)


async def create_user(data: UserDTO.UserCred, db: AsyncSession) -> UserDTO.UserRole:
    data.password = hash_context.hash(data.password)
    user = User(**data.dict())
    try:
        db.add(user)
        await db.commit()
        await db.refresh(user)
    except Exception as e:
        print(e)

    return UserDTO.UserRole(**user.__dict__)


async def get_user(login: str, db: AsyncSession) -> UserDTO.User | None:
    query = select(User).where(User.login == login)
    user = await db.execute(query)
    user = user.scalar_one_or_none()
    if user is None:
        return None
    return UserDTO.User(**user.__dict__)


async def delete_user(login: str, db: AsyncSession) -> None:
    query = delete(User).where(User.login == login)
    await db.execute(query)
    await db.commit()

