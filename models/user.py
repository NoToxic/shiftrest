from sqlalchemy import Column, Integer, String, Numeric, Date, Boolean
from datetime import date, timedelta
from database import Base


class User(Base):
    __tablename__ = "user"

    @staticmethod
    def gen_valid_until():
        valid_until = date.today() + timedelta(days=15)
        return valid_until

    id = Column(Integer, primary_key=True, autoincrement=True)

    login = Column(String, nullable=False, unique=True)
    password = Column(String, nullable=False)

    salary = Column(Numeric(10, 2), nullable=False, default=40_000)
    next_update = Column(Date, nullable=False, default=gen_valid_until)

    is_superuser = Column(Boolean, default=False)
