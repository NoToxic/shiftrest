from passlib.context import CryptContext
import os

schemes = os.getenv("HASH_SCHEMES").split(",")

myctx = CryptContext(schemes=schemes)
